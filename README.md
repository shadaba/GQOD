This directory contains the public release with GQOD model.
This resource was released along with this paper arxiv.



#### The clustering measurements are in the directory data/
QSO-auto.wp      : QSO auto correlation

QSOxLRG-cross.wp : QSO cross correlation with LRG

QSOxELG-cross.wp : QSO cross correlation with ELG

fQSO-delta.wp    : Fraction of QSO with over density environment


#### The MTHOD mock catalogue used is in directory MTHOD-mock/
The default galaxy catalogue is provided but one can use a different galaxy catalogue if of interest.
The catalogue itself is in fits format. This is specifiec in the ini file of FitQSOHOD.py.


##### The code is in the directory GQOD/


##### You will need to install following python libraries:
numpy

fitsio

emcee

corrfunc



##### Likelihood analysis:
step 1: move to GQOD directory, it is needed because all relative paths aredefined assuming you are in this directory

cd GQOD

step 2: Now excute the mcmc as follows

python -u FitQSOHOD.py -config_file InitQSO/HODELG_modelHM-4-GAL4.ini -action 1 -burnin 5 -nsteps 8000 -nwalker 200 -storetime 1

Step 3: Analysing chains

The output chains are self explanatory. You can analyze them using the python notebook Analyze_QSO_HOD.ipynb . Note that this notebook might not execute due to additional dependency with my local code. It is only intended to be used as instructional guide but if you are interested in having an executeable version then please get in touch and I will try to add all the additional dependency to this repository.

