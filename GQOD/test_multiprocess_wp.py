from __future__ import print_function
import numpy as np
import pylab as pl

import fitsio as F

import sys
sys.path.insert(0,'/home/salam/Projects/eBOSS/QSOHOD_models/')
import Model_functions as MF


print('loading fits')
HODdir='/disk2/salam/eBOSS_HOD_chains/MDPL2_HOD_cat/FITS/ValueAdded_cat/'
HODfile='MDPL2-DR16LRG-DR16QSO-ELGv4-bfit-ELGHOD-nosubhalo_nozsim_zRSD-subeBOSSdensity-eBOSS-s5-g5-gdd-wP-gal.fits'
#openfits file
fin=F.FITS(HODdir+HODfile)


print('loading QSO object')
cobj=MF.QSO_models_corr(fin,nLM=25,nQSO=20000,nbin=10,cat='mock')
indsel=cobj.fin[1].where('galtype==1')
indsel=np.random.choice(indsel,20000,replace=False)
indsel.sort()


print('computing wp without mp')
wpmodel_nomp=cobj.wp_sample(index_QSO=indsel,autoQSO=1,crossELG=1,crossLRG=1,rsd='real',
                           multiprocess=0)
print('computing wp wit mp')
wpmodel_wmp=cobj.wp_sample(index_QSO=indsel,autoQSO=1,crossELG=1,crossLRG=1,rsd='real',
                           multiprocess=1)


print(wpmodel_nomp.keys())

for tt,tkey in enumerate(wpmodel_nomp.keys()):
    pl.plot(wpmodel_nomp[tkey][:,0],wpmodel_nomp[tkey][:,1],'x',label=tkey+'-nomp')
    pl.plot(wpmodel_nomp[tkey][:,0],wpmodel_nomp[tkey][:,1],'.',label=tkey+'-nomp')

pl.xscale('log')
pl.yscale('log')
pl.legend()
pl.show()
