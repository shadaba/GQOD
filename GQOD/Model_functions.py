#This has a class which handle all the model related issues
#Look at eBOSS ELG v2 catalog
from __future__ import print_function

import numpy as np
import os
import sys

#for clustering measurement
#for stacpolly
sys.path.insert(0,'/home/salam/Projects/MultiHODFitter/')
#for fyne
#sys.path.insert(0,'/disk1/salam/Projects/MultiHODFitter/')
import Measure_wp_gcat as Mwp

import fitsio as F
import multiprocessing as mp


from scipy.special import erfc,erf

#This class handles all the detailes step by step
class QSO_models_corr:
    def __init__(self,fin,nLM=25,nQSO=20000,modelGAL=1,modelHM=1,
                                nbin=10,rs=5,cat='mock',nofsat=0,speedtest=0):
        self.fin=fin
        #This is needed for the galaxy fraction part
        self.cat = cat
        self.nbin= nbin    

        #if fsat parameter shouldn't be used then set nofsat=1
        #This means the probability of fon for QSO is independent of central/sat distinction
        self.nofsat=nofsat
        if(self.nofsat==0):
            #if nofsat=0 -> mean fsat is a parameter and hence we need both cen and sat seperately
            self.gpos_need=['cen','sat']
        elif(self.nofsat==1):
            #in this case both cen and sat are used together
            self.gpos_need=['censat']


        #set the models for HM and GAL
        self.modelGAL=modelGAL; self.select_GAL()
        self.modelHM=modelHM

        #Number of total QSO
        self.nQSO=np.int(nQSO)
        
        #halo mass range need to cover
        self.nLM=nLM
        LM_min=np.log10(np.min(self.fin[1]['Mhalo'][:]))
        LM_max=np.log10(np.max(self.fin[1]['Mhalo'][:]))
        self.LMhalo_ed=np.linspace(LM_min,LM_max,self.nLM+1)
        self.LMhalo_mid=0.5*(self.LMhalo_ed[1:]+self.LMhalo_ed[:-1])
        self.dLMhalo=0.5*np.mean(self.LMhalo_ed[1:]-self.LMhalo_ed[:-1])
        
        #self.model=model #This sets the model to use for QSO
 
        #galaxiekey
        self.gal_keys={'LRG':1,'QSO':2,'ELG':3}
        
        #Get the boolean LRG and ELG indices
        galtype=self.fin[1]['galtype'][:]
        self.bool_index={'LRG':galtype==1,
                        'ELG':galtype==3}


        #make everything in self.index
        self.indsel=self.fin[1].where('1>0')
        #update the logdel
        self.get_logdel(indsel=self.indsel,rs=rs)
        self.weight=np.ones(self.indsel.size)


        if(speedtest==0):#These are slow steps and not always needed while testing
           #setup the indices for efficient QSO sampling
           self.setup_galindex()
            
           #setup the galaxy histogram for efficient QSO number calcualtion with halo mass
           self.setup_galhist()
        
           #setup the x,y,z and vx values in numpy array
           self.setup_coordinate()
        
        return
    
    def setup_coordinate(self):
        self.xx=self.fin[1]['xx'][:]
        self.yy=self.fin[1]['yy'][:]
        self.zz=self.fin[1]['zz'][:]
        self.vz=self.fin[1]['vz'][:]
        
        return
    
    def setup_galindex(self):
        '''compute the galaxy histogram for speedy use in model'''
        self.gal_index={}
        #halomass
        LMhalo=np.log10(self.fin[1]['Mhalo'][:])
        for tracer in ['LRG','ELG']:
            self.gal_index[tracer]={}
            sel_tr='galtype==%d'%(self.gal_keys[tracer])
            for gpos in ['cen','sat']:
                self.gal_index[tracer][gpos]={}
                if(gpos=='cen'):
                    sel_str='%s && central==1'%sel_tr
                else:
                    sel_str='%s && central!=1'%sel_tr
                    
                for ii in range(0,self.nLM):
                    Mh1=np.power(10,self.LMhalo_ed[ii])
                    Mh2=np.power(10,self.LMhalo_ed[ii+1])
                    selHM_str='Mhalo> %f && Mhalo <=%f'%(Mh1,Mh2)
                    
                    sel_all='%s && %s'%(sel_str,selHM_str)
                    self.gal_index[tracer][gpos][ii]=self.fin[1].where(sel_all)
                
                
        #combined LRG and ELG
        self.gal_index['LRGELG']={}
        for gpos in ['cen','sat']:
            self.gal_index['LRGELG'][gpos]={}
            for ii in range(0,self.nLM):
                self.gal_index['LRGELG'][gpos][ii]=np.append(self.gal_index['LRG'][gpos][ii],
                                                            self.gal_index['ELG'][gpos][ii])

        #if the main model require censat which is nofsat=1 then create that
        for tracer in self.tracer_need:
            self.gal_index[tracer]['censat']={}
            for ii in range(0,self.nLM):
                self.gal_index[tracer]['censat'][ii]=np.append(self.gal_index[tracer]['cen'][ii],
                                                          self.gal_index[tracer]['sat'][ii])
        return
    
    def setup_galhist(self):
        '''compute the galaxy histogram for speedy use in model'''
        self.gal_hist={}
        #halomass
        LMhalo=np.log10(self.fin[1]['Mhalo'][:])
        for tracer in ['LRG','ELG']:
            self.gal_hist[tracer]={}
            sel_tr='galtype==%d'%(self.gal_keys[tracer])
            for gpos in ['cen','sat']:
                if(gpos=='cen'):
                    sel_str='%s && central==1'%sel_tr
                else:
                    sel_str='%s && central!=1'%sel_tr
                indsel=self.fin[1].where(sel_str)
                hh=np.histogram(LMhalo[indsel],bins=self.LMhalo_ed)
                self.gal_hist[tracer][gpos]=hh[0]
                
        #combined LRG and ELG
        self.gal_hist['LRGELG']={'cen':self.gal_hist['LRG']['cen']+self.gal_hist['ELG']['cen'],
                                'sat':self.gal_hist['LRG']['sat']+self.gal_hist['ELG']['sat']}

        #if the main model require censat which is nofsat=1 then create that
        for tracer in self.tracer_need:
            self.gal_hist[tracer]['censat']=self.gal_hist[tracer]['cen']+self.gal_hist[tracer]['sat']
        
        return
   
    def setup_spline(self,outroot='',version=0,verbose=1):
        '''This sets up the knots for spline'''         
        self.spline_setup=1
        self.spline_version=version
        if(version==0):
            if(self.modelGAL in [1,2,4]):
                self.spline_knots=np.array([11.5,12.0,12.25,12.5,12.75,13.0,13.5,15.2])
            elif(self.modelGAL==3):
                self.spline_knots=np.array([12.0,12.25,12.5,12.75,13.0,13.5,15.2])
            self.knot_num=np.arange(0,self.spline_knots.size)
        elif(version==1):
            self.free_knots=np.array([11.2,11.8,12.25,12.75,13.0,13.5,15.2])
            self.norm_knot=12.5
            self.spline_knots=np.zeros(self.free_knots.size+1)

            self.knot_num=[];count=0;normflag=0
            for ii in range(0,self.free_knots.size):
                if(self.free_knots[ii]>self.norm_knot and normflag==0):
                    self.spline_knots[count]=self.norm_knot
                    self.norm_num=count
                    count=count+1
                    normflag=1

                self.knot_num.append(count)
                self.spline_knots[count]=self.free_knots[ii]
                count=count+1

            if(verbose==1):
                print('spline knots: ',self.spline_knots)
                print('This knot is used to normalize and not free: value , num',self.norm_knot,self.knot_num)
        elif(self.spline_version==2):
            self.spline_knots=np.array([11.2,11.8,12.25,12.5,12.75,13.0,13.5,15.2])
            self.knot_num=np.arange(0,self.spline_knots.size)

        self.nspline=self.spline_knots.size
        if(verbose==1): 
            print('setting up spline with following knot position in log10(Mhalo)')
            print(self.spline_knots)
            print('spline num:',self.knot_num)
        if(outroot!=''):
            fname=outroot+'.splineknots'
            with open(fname,'w') as fout:
                for ii in range(0,self.nspline):
                    fout.write('Mhknot%d %4.2f\n'%(ii,self.spline_knots[ii]))
            print('written knot positions: %s'%fname)


        return

    def Reset_modelHM_modelGAL(self,modelHM=1,modelGAL=1,version=0,verbose=0,nofsat=-1):
        self.modelGAL=modelGAL; self.select_GAL()
        self.modelHM=modelHM
        if(self.modelHM==4):
           self.setup_spline(outroot='',version=version,verbose=verbose)

        #set nofsat only if its different from -1
        if(self.nofsat!=-1):
            self.nofsat=nofsat
            if(self.nofsat==0):
                #if nofsat=0 -> mean fsat is a parameter and hence we need both cen and sat seperately
                self.gpos_need=['cen','sat']
            elif(self.nofsat==1):
                #in this case both cen and sat are used together
                self.gpos_need=['censat']
                #if the main model require censat which is nofsat=1 then create that
                for tracer in self.tracer_need:
                    #for the histogram
                    if('censat' not in self.gal_hist[tracer].keys()):
                        self.gal_hist[tracer]['censat']=self.gal_hist[tracer]['cen']+self.gal_hist[tracer]['sat']
                    #for the indices
                    if('censat' not in self.gal_index[tracer].keys()):
                        self.gal_index[tracer]['censat']={}
                        for ii in range(0,self.nLM):
                            self.gal_index[tracer]['censat'][ii]=np.append(self.gal_index[tracer]['cen'][ii],
                                                                self.gal_index[tracer]['sat'][ii])

        return


    def model_QSO_probability(self,pdict={}):
        '''This assigns the QSO based on model from LRG and ELG'''

        #based on model get the number of QSO probability of central and satelite with halo mass
        if(self.modelHM==1): #Apply first model
            #model one assumes there is only one mass cut with sign to select QSO 
            #with halo mass
            Mdiff=(pdict['LMcut']-self.LMhalo_mid)/(np.sqrt(2.0)*pdict['sigmaM'])
            P_cen = 0.5 *erfc(Mdiff)
            P_sat=np.copy(P_cen)
        elif(self.modelHM==2):
            Mdiff=(pdict['LMcut']-self.LMhalo_mid)/(np.sqrt(2.0)*pdict['sigmaM'])
            P_cen=np.exp(-np.power(Mdiff,2))
            P_sat=np.copy(P_cen)
        elif(self.modelHM==3):
            Mdiff=(pdict['LMcut']-self.LMhalo_mid)/(np.sqrt(2.0)*pdict['sigmaM'])
            normdist=np.exp(-np.power(Mdiff,2))
            cdfnorm=(1.0+erf(-pdict['gamma']*Mdiff))
            skewnorm=2.0*normdist*cdfnorm
            P_cen=skewnorm
            P_sat=np.copy(P_cen)
        elif(self.modelHM==4): #spline form of fon
            pknots=np.zeros(self.nspline)
            for ii in self.knot_num:
               pknots[ii]=pdict['Mhknot%d'%ii]
            if(self.spline_version==1):
               #get the norm knot with normalization
               pknots[self.norm_num]=1.0-np.sum(pknots)
            #use linearspline
            P_cen=np.interp(self.LMhalo_mid,self.spline_knots,pknots,left=0,right=0)
            P_sat=np.copy(P_cen)

        #Normalize the probability
        #P_cen=P_cen/np.sum(P_cen)
        #P_sat=P_sat/np.sum(P_sat)

        return P_cen,P_sat



    def get_max_censat(self,prob,galcount=''):
        '''compute the maximum number of central satellite possible for this proabbility distribution'''
    
        ind_max=np.argmax(prob)
        if(prob[ind_max]==0):
            return 0
        prob=prob/prob[ind_max]
        N_HM=galcount*prob
        max_count=np.sum(N_HM)
    
        return max_count


    def select_GAL(self):
        '''returns the galaxy sample parent to QSO tag'''
        #select tracer based on modelGAL whether to select from LRG,ELG or LRGELG
        if(self.modelGAL==1): #no different between LRG and ELG
            self.model_tr='LRGELG'; self.tracer_need=['LRGELG']
        elif(self.modelGAL==2): #QSO is selected only from ELG
            self.model_tr='ELG'; self.tracer_need=['ELG']
        elif(self.modelGAL==3): #QSO is selected only from LRG
            self.model_tr='LRG'; self.tracer_need=['LRG']
        elif(self.modelGAL==4): #mix of LRG and ELG with proportion given by fLRG
            self.tracer_need=['LRG','ELG']

        return

    def model_select_QSO_index(self,pdict={}):
        '''Transform ELG and LRG to QSOs This is for modelGAL 1,2,3 see seperate function for 4'''

        #get the probability distribution
        P_cen,P_sat=self.model_QSO_probability(pdict=pdict)
        Pon={'cen':P_cen, 'sat':P_sat, 'censat':P_cen}

        #decide number of QSo from central and satelite as the function of Halo mass
        #Number of satellite
        if(self.nofsat==0):
           Ngal={'sat':self.nQSO*pdict['fsat']}
           Ngal['cen']=self.nQSO-Ngal['sat']
        else:
           Ngal={'censat': self.nQSO}

        
        #Test if we can have enough QSO
        penalty=0
        for gg,gpos in enumerate(self.gpos_need):
            max_gal=self.get_max_censat(Pon[gpos],galcount=self.gal_hist[self.model_tr][gpos])
            
            #if these numbers are less than Ncen/Nsat then its not possible to get the QSO population
            if(max_gal<Ngal[gpos]):
               #In this case we should penalize the mcmc and wp wont be needed
               penalty=penalty+max(0,Ngal[gpos]-max_gal)

        if(penalty>0): 
            penalty=10000+penalty
            return np.array([]), penalty

            
        #get the counts
        Ngal_HM={}
        for gg,gpos in enumerate(self.gpos_need):
            Ngal_HM[gpos]=self.gal_hist[self.model_tr][gpos]*Pon[gpos]
            #normalize
            Ngal_HM[gpos]=Ngal[gpos]*Ngal_HM[gpos]/np.sum(Ngal_HM[gpos])
            #convert them to integer
            Ngal_HM[gpos]=Ngal_HM[gpos].astype(int)


        #list to host QSO index
        index_QSO=np.array([],dtype=int)
        #This is where actual indices are selected

        for ii in range(0,self.nLM):
            for gg,gpos in enumerate(self.gpos_need):
                if(Ngal_HM[gpos][ii]!=0):
                    #sample randomly for QSO turnon of central
                    ind_QSO_sel=np.random.choice(self.gal_index[self.model_tr][gpos][ii],
                        Ngal_HM[gpos][ii], replace=False )
                    #append to global indices
                    index_QSO=np.append(index_QSO,ind_QSO_sel)

            #print(ii,ind_cen.size,Ncen_HM[ii],ind_sat.size,Nsat_HM[ii])

        #convert index list to array
        index_QSO.sort()

        return index_QSO,0

    def model_select_QSO_index_GAL4(self,pdict={}):
        '''Transform ELG and LRG to QSOs, 
           This is only for modelGAL4 as it mixes the LRG and ELG in some ratio'''

        #get the probability distribution
        P_cen,P_sat=self.model_QSO_probability(pdict=pdict)
        Pon={'cen':P_cen, 'sat':P_sat, 'censat':P_cen}

        #decide number of QSO from central and satelite as the function of Halo mass
        #Number of satellite
        if(self.nofsat==0):
           Ngal={'sat':self.nQSO*pdict['fsat']}
           Ngal['cen']=self.nQSO-Ngal['sat']
        else:
           Ngal={'censat': self.nQSO}

       
        fgal={'LRG':pdict['fLRG']}
        fgal['ELG']=1.0-fgal['LRG']

        #Test if we can have enough QSO
        penalty=0
        for gg,gpos in enumerate(self.gpos_need):
            #total counts
            tothist=self.gal_hist['LRG'][gpos]*fgal['LRG']+self.gal_hist['ELG'][gpos]*fgal['ELG']
            max_gal=self.get_max_censat(Pon[gpos],galcount=tothist)
            
            #if these numbers are less than Ncen/Nsat then its not possible to get the QSO population
            if(max_gal<Ngal[gpos]):
               #In this case we should penalize the mcmc and wp wont be needed
               penalty=penalty+max(0,Ngal[gpos]-max_gal)

        if(penalty>0): 
            penalty=10000000+penalty
            return np.array([]), penalty

            
        #get the counts
        Ngal_HM={}
        for tracer in ['LRG','ELG']:
            Ngal_HM[tracer]={}
            for gg,gpos in enumerate(self.gpos_need):
                Ngal_HM[tracer][gpos]=self.gal_hist[tracer][gpos]*Pon[gpos]*fgal[tracer]


        #normalize
        for gg,gpos in enumerate(self.gpos_need):
            Ngal_norm=1.0*np.sum(Ngal_HM['LRG'][gpos]+Ngal_HM['ELG'][gpos])/Ngal[gpos]
            for tracer in ['LRG','ELG']:
               Ngal_HM[tracer][gpos]=Ngal_HM[tracer][gpos]/Ngal_norm
               #convert them to integer
               Ngal_HM[tracer][gpos]=Ngal_HM[tracer][gpos].astype(int)

        #list to host QSO index
        index_QSO=np.array([],dtype=int)
        #This is where actual indices are selected

        for ii in range(0,self.nLM):
            for tracer in ['LRG','ELG']:
                for gg,gpos in enumerate(self.gpos_need):
                    if(Ngal_HM[tracer][gpos][ii]!=0):
                        #sample randomly for QSO turnon
                        ind_QSO_sel=np.random.choice(self.gal_index[tracer][gpos][ii],
                            Ngal_HM[tracer][gpos][ii], replace=False )
                        #append to global indices
                        index_QSO=np.append(index_QSO,ind_QSO_sel)

        #convert index list to array
        index_QSO.sort()

        return index_QSO,0
    
   
    def wp_multiprocess(self,SHOD,LD,data2='',xitype='',multiprocess=0):
        '''This wraps the thing in subprocess to limit the memory leak'''

        '''Due to the use of multiprocessing I must not use threading'''
        SHOD['nthreads']=1
        if(multiprocess==1):
            def wp_wrap(SHOD,LD,data2,xitype,out_q):
                SHOD,LD=Mwp.get_wp(SHOD,LD,data2=data2,xitype=xitype)
                out_q.put(LD['model'])

            q = mp.Queue()
            p = mp.Process(target=wp_wrap, args=(SHOD,LD,data2,xitype,q))
            p.start()
            thiswp=q.get()    # This gets the wp
            p.join()
        else:
            SHOD,LD=Mwp.get_wp(SHOD,LD,data2=data2,xitype=xitype)
            thiswp=LD['model']

        return thiswp

    #Get wp calculation efficiently for various fraction   
    def wp_sample(self,index_QSO='',autoQSO=1,crossELG=0,crossLRG=0,rsd='zRSD',multiprocess=0):
        '''Measure the auto and cross wp for QSO'''

        #setup wp parameters
        SHOD={'redshift':0,'Lbox':1000.0,
               'wp_rper':[-1.0, 1.477, 25],'wp_rpi':[-40.0,40.0],
               'nthreads':4}

        #Now get the final catalogues
        LD={}; wpmodel={}

        LD['gcat']=np.column_stack([self.xx[index_QSO],self.yy[index_QSO],self.zz[index_QSO],self.vz[index_QSO]])
        if(rsd=='zRSD'):#redshift space position
            LD['gcat'][:,2]=np.mod(LD['gcat'][:,2]+(LD['gcat'][:,3]/100.0),SHOD['Lbox'])

        if(autoQSO==1):
            #To get the auto correlation of first sample
            #SHOD,LD=Mwp.get_wp(SHOD,LD,data2='',xitype='auto')
            thiswp=self.wp_multiprocess(SHOD,LD,data2='',xitype='auto',multiprocess=multiprocess)
            #Now store it in wp
            wpmodel['QSO-auto']=np.copy(thiswp)
            

        if(crossLRG==1 or crossELG==1):
            ind_noQSO=np.ones(self.bool_index['LRG'].size,dtype=bool)
            ind_noQSO[index_QSO]=False

            if(crossLRG==1):
                ind_LRG=self.bool_index['LRG']*ind_noQSO

                LD['gcat2']=np.column_stack([self.xx[ind_LRG],self.yy[ind_LRG],self.zz[ind_LRG],self.zz[ind_LRG]])
                if(rsd=='zRSD'):
                    LD['gcat2'][:,2]=np.mod(LD['gcat2'][:,2]+(LD['gcat2'][:,3]/100.0),SHOD['Lbox'])
                #To get the cross correlation
                #SHOD,LD=Mwp.get_wp(SHOD,LD,data2=LD['gcat2'],xitype='cross')
                thiswp=self.wp_multiprocess(SHOD,LD,data2=LD['gcat2'],xitype='cross',multiprocess=multiprocess)
                #Now store it in wp
                wpmodel['LRG-cross']=np.copy(thiswp)

            if(crossELG==1):
                ind_ELG=self.bool_index['ELG']*ind_noQSO
                LD['gcat2']=np.column_stack([self.xx[ind_ELG],self.yy[ind_ELG],self.zz[ind_ELG],self.zz[ind_ELG]])
                if(rsd=='zRSD'):
                    LD['gcat2'][:,2]=np.mod(LD['gcat2'][:,2]+(LD['gcat2'][:,3]/100.0),SHOD['Lbox'])

                #To get the cross correlation
                #SHOD,LD=Mwp.get_wp(SHOD,LD,data2=LD['gcat2'],xitype='cross')
                thiswp=self.wp_multiprocess(SHOD,LD,data2=LD['gcat2'],xitype='cross',multiprocess=multiprocess)
                #Now store it in wp
                wpmodel['ELG-cross']=np.copy(thiswp)

        #print('LRG, ELG,QSO',np.sum(ind_LRG),np.sum(ind_ELG),index_QSO.size)
        return wpmodel


###additional function to get the environmental dependent QSO fraction
    def get_delta(self,indsel=np.array([]),rs=8):
        if(indsel.size!=0):
            self.indsel=indsel
        
        rhos=self.fin[1]['rhoSrad%d'%rs][self.indsel]
        rhos=rhos/np.mean(self.fin[1]['rhoSrad%d'%rs][:])
        delta_rs=rhos-1.0

        return delta_rs

    
    def get_logdel(self,indsel=np.array([]),rs=5):
        
        if(self.cat =='data'):
            delta=self.get_delta_wz(indsel=indsel,rs=rs)
        else:
            delta=self.get_delta(indsel=indsel,rs=rs)
        
        self.logdel=np.log10(1+delta)
        self.gal_type=self.fin[1]['galtype'][self.indsel]
        
        #percentile binning in logdelta
        #self.logdel_bin=np.percentile(self.logdel,np.linspace(0,100,self.nbin+1))
        #linear bins
        self.logdel_bin=np.linspace(-1,1,self.nbin+1)
        
        self.logdel_mid=np.zeros(self.nbin)
        for ii in range(0,self.nbin):
            ind1=self.logdel>=self.logdel_bin[ii]
            ind2=self.logdel<self.logdel_bin[ii+1]
            self.logdel_mid[ii]=np.mean(self.logdel[ind1*ind2])
            
        return 
    
    def get_Envindex(self,env):
        '''env is one of 'All','void','sheet','filament','knot' '''
        #select environments
        #env_list=['All','void','sheet','filament','knot']
        #env_dic={}
        #for ee, env in enumerate(env_list):
        if(env=='All'):
            index=np.ones(self.indsel.size,dtype=bool)
        else:
            p_this=self.fin[1]['P_%s'%env][self.indsel]
            index=p_this>0.9
        return index
    
        
    def get_countgal_env(self,indQSO=np.array([]),env_list=[],werr=0):
        

        #QSO index in boolean
        indQSO_bool=np.zeros(self.indsel.size,dtype=bool)
        indQSO_bool[indQSO]=1
          
        if(env_list==[]):
            self.env_list=['All']#,'void','sheet','filament','knot']
          

        
        gal_subrho={'LRG':1.0e-4,'QSO':1.0e-5,'ELG':5.0e-4}

        if(werr==1):
            jnid=self.fin[1]['jn'][self.indsel]
            njn=jnid.max()
            bjn=3 #first three index to store mean and err and all
            self.werr=1
            self.njn=njn
            
        self.countgal_dic={}
        for ee,env in enumerate(self.env_list):
            if(werr==0):
                self.countgal_dic[env]=np.zeros(self.nbin*2).reshape(self.nbin,2)
            else:
                self.countgal_dic[env]=np.zeros(self.nbin*(njn+bjn)).reshape(self.nbin,njn+bjn)

        #measure number of galaxy in each bin of each type
        for ii in range(0,self.nbin):
            ind1=self.logdel>=self.logdel_bin[ii]
            ind2=self.logdel<self.logdel_bin[ii+1]

            for ee,env in enumerate(self.env_list):
                index=self.get_Envindex(env)
                indall=ind1*ind2*index
                if(werr==0):
                    #QSO count
                    self.countgal_dic[env][ii,0]=np.sum(self.weight[indall*indQSO_bool])
                    #All count
                    self.countgal_dic[env][ii,1]=np.sum(self.weight[indall])
                elif(werr==1):
                    self.countgal_dic[env][ii,0]=np.sum(self.weight[indall])
                    for jn in range(0,njn):
                        ind_jn=jnid==jn
                        self.countgal_dic[env][ii,bjn+jn]=np.sum(self.weight[indall*ind_jn])
                    #now subtract the accumalted jackknifes
                    self.countgal_dic[env][ii,bjn:]=self.countgal_dic[env][ii,0]-self.countgal_dic[env][ii,bjn:]
                    #calculate mean and error
                    self.countgal_dic[env][ii,1]=np.mean(self.countgal_dic[env][ii,bjn:])
                    self.countgal_dic[env][ii,2]=np.std(self.countgal_dic[env][ii,bjn:])*np.sqrt(njn-1)
          
                        
        return
    
    
### This is a function to compute the wp as te function of delta split in 5 bins
    def wp_sample_delta(self,index_QSO='',rs=5,rsd='zRSD'):
        '''Measure the auto and cross wp for QSO,
        Be extra careful with RSD setting as the fits file have already rsd applied in which
        case you should use rsd=real to avoid applying RSD twice'''

        #setup wp parameters
        SHOD={'redshift':0,'Lbox':1000.0,
               'wp_rper':[-1.0, 1.477, 25],'wp_rpi':[-40.0,40.0],
               'nthreads':4}

        selections=['crossAll','0-25','25-50','50-75','75-100']

        #get the log delta
        self.get_logdel(indsel=index_QSO,rs=rs)


        #Now get the final catalogues
        LD={}; wpmodel={}

        #first catalog of everything which is all LRG and ELG only remove initial QSO
        ind_base=self.fin[1].where('galtype!=2')
        LD['gcat']=np.column_stack([self.xx[ind_base],self.yy[ind_base],
                                  self.zz[ind_base],self.vz[ind_base]])


        if(rsd=='zRSD'):#redshift space position
            LD['gcat'][:,2]=np.mod(LD['gcat'][:,2]+(LD['gcat'][:,3]/100.0),SHOD['Lbox'])

        for ss,sel in enumerate(selections):
            if(sel=='crossAll'):
                #To get the auto correlation of first sample
                SHOD,LD=Mwp.get_wp(SHOD,LD,data2='',xitype='auto')
                #Now store it in wp
                wpmodel[sel]=np.copy(LD['model'])
            else:
                #get the percentile lims
                sel_spl=sel.split('-')
                perc_lim=[np.float(sel_spl[0]),np.float(sel_spl[1])]
                del_lims=np.percentile(self.logdel,perc_lim)
                #apply delta limits
                ind1=self.logdel>=del_lims[0]
                ind2=self.logdel<del_lims[1]
                ind_this=index_QSO[ind1*ind2] 
                LD['gcat2']=np.column_stack([self.xx[ind_this],self.yy[ind_this],
                                  self.zz[ind_this],self.zz[ind_this]])
                if(rsd=='zRSD'):
                    LD['gcat2'][:,2]=np.mod(LD['gcat2'][:,2]+(LD['gcat2'][:,3]/100.0),SHOD['Lbox'])
                #To get the cross correlation
                SHOD,LD=Mwp.get_wp(SHOD,LD,data2=LD['gcat2'],xitype='cross')
                #Now store it in wp
                wpmodel[sel]=np.copy(LD['model'])

        return wpmodel

