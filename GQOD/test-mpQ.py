from multiprocessing import Process, Queue
import General_function_QSOHOD as GFQ
import os


def f(ii,q):
    q.put([ii, None, 'hello',os.getpid()])

if __name__ == '__main__':
    print(GFQ.ltime())
    #q = Queue()
    for ii in range(0,1000):
       q = Queue()
       p = Process(target=f, args=(ii,q))
       p.start()
       #print(q.get())    # prints "[42, None, 'hello']"
       res=q.get()  # prints "[42, None, 'hello']"
       p.join()

    print(os.getpid())
    print(GFQ.ltime())
