#Author: Shadab Alam, April 2019
#fit the QSO HOD models
#

from __future__ import print_function

import os
import sys

import emcee


#import triangle
import numpy as np
import scipy.optimize as op

#This holds the definition of class for QSO HOD including wp measurement functions
import Model_functions as MF
# This hold all other function outside class
import General_function_QSOHOD as GFQ

import fitsio as F
import time

#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as pl

import argparse


if __name__=="__main__":
   parser = argparse.ArgumentParser(description='Fit QSO HOD model')
   #these should be input
   parser.add_argument('-config_file',default='test.ini')
   parser.add_argument('-plots',type=int,default=1)
   parser.add_argument('-sav'  ,type=int,default=1)

   parser.add_argument('-modelGAL' ,type=int,default=-1,help='If this is not -1 then this will be used inplace of one from ini file')
   #parser.add_argument('-outroot' ,default='test/test')
   parser.add_argument('-burnin' ,type=int,default=10)
   parser.add_argument('-nwalker',type=int,default=100)
   parser.add_argument('-nsteps',type=int,default=5)
   parser.add_argument('-action',type=int,default=2,help='''action=1 run chains, \n action=2 test likelihoood, \n''')
   parser.add_argument('-storetime',type=int,default=0,help='''set 1 to store time \n''')

   #action=3 get the parameters variation, if stats file exists then it will load it and use the bestfit parameter as the fixed point for parameter dependence\n action=4 minimize using scipy optimize''')


   args = parser.parse_args()

   # Reproducible results!
   np.random.seed(123)
   print('initializing random seed')

   #get the pid of this process
   import psutil
   pid = os.getpid()
   py = psutil.Process(pid)
   time_beg=time.localtime()
   print('my pid=',pid,GFQ.ltime())

def get_memoryuse(py,unit='MB'):
    if(unit=='MB'):
        return py.memory_info()[0]/2.**20
    elif(unit=='GB'):
        return py.memory_info()[0]/2.**30
    

def print_dictionary(fout,dicin,dicname='HOD',indent=''):
    #The print everything in dictionary except array too big
    fout.write(indent+'###dictionary=%s###\n'%(dicname))
    for ii,tkey in enumerate(dicin.keys()):
        if(isinstance(dicin[tkey],dict)): #check if its dictionary then make a recursive call
            print_dictionary(fout,dicin[tkey],dicname=dicname+'>'+tkey,indent=indent+'   ')
        elif(isinstance(dicin[tkey],np.ndarray)): #if its an array
            #get the array shape
            tshape=dicin[tkey].shape
            tshapestr=tkey+' : '
            for ss in tshape:
               tshapestr='%s %d'%(tshapestr,ss)
            fout.write(indent+tshapestr+'\n')
            #if size less than 100 then print all elements
            if(dicin[tkey].size<100):
               np.savetxt(fout,dicin[tkey],fmt='%12.8f')
            else: #if size more than 100 then print shape and few elements
               tmparr=dicin[tkey].reshape(dicin[tkey].size)
               tstr1='* First Five:'
               tstr2='* Last  Five:'
               for tt in range(0,5):
                   tstr1='%s %12.8e'%(tstr1,tmparr[ii])
                   tstr2='%s %12.8e'%(tstr2,tmparr[-ii])
               fout.write(indent+tstr1+'\n')
               fout.write(indent+tstr2+'\n')
        elif(isinstance(dicin[tkey],list)): #if list then print the list
            fout.write(indent+tkey+' : ')
            for tmem in dicin[tkey]:
               if(isinstance(tmem,str)):
                   fout.write('%s '%tmem)
               else:
                   fout.write('%f '%tmem)
            fout.write('\n')
        elif(isinstance(dicin[tkey],str)): #if its a string
            fout.write('%s %s : %s\n'%(indent,tkey,dicin[tkey]))
        else: #otherwise its either a integet or float
            try:
               fout.write('%s %s : %f\n'%(indent,tkey,dicin[tkey]))
            except:
               if(tkey not in ['int_r200', 'int_rsr200']):
                  print('** cant write it in output file:',tkey,dicin[tkey])

    return 0

def write_minimizer_result(res,foutname):
    with open(foutname,'wb') as fout:
        fout.write('#results of minimizer\n')
        fout.write('#final_simplex 1: \n')
        for ff,tp in enumerate(res.final_simplex):
            tpshape=np.copy(tp.shape)
            tp=tp.reshape(tp.size)
            for ii in range(0,tp.size):
                fout.write('%f '%tp[ii])
                if(len(tpshape)>1):
                    if((ii+1)%tpshape[1]==0):
                        fout.write('\n')

            if(ff==0):
                fout.write('\n#final_simplex 2:\n')
        fout.write('\n#fun     : %f'%res.fun)
        fout.write('\n#message : %s'%res.message)
        fout.write('\n#nfev    : %d'%res.nfev)
        fout.write('\n#nit     : %d'%res.nit)
        fout.write('\n#staus   : %d'%res.status)
        fout.write('\n#success : %s'%res.success)
        fout.write('\n#Best fit parameters: ')
        for ii in range(0,res.x.size):
            fout.write('%f '%res.x[ii])

    print('written minimizer result in file: %s'%foutname)
    return 0

#This function runs scipy minimizer and saves the output in appropriate file
def run_minimizer(SHOD):
   #imports the appropriate function first from scipy
   from scipy.optimize import minimize

   #files to write output of minimzation
   #Initiate the chain files
   chainfile=SHOD['outroot']+'.minimizer'
   if(os.path.isfile(chainfile)):
      print('The minimizer file already exists (exiting): %s'%chainfile)
      sys.exit()
   else:
      #initiate chain file
      fchain=open(chainfile,'w')
      labs='#pars: weight, loglike '
      jj=0
      for pp in SHOD['parlist']:
         if(SHOD[pp][3]>=0):
            labs=labs+', '+pp
            jj=jj+1

      #also writes derived parameter
      labs=labs+', Ngal*'
      fchain.write(labs+'\n')
      fchain.close()
      print('Created minimizer file: %s \n'%chainfile)
      SHOD['minfile']=chainfile

   #setup initial guess in pos0
   print('### Initial guess of parameters ###\n')
   pos0=[]
   for pp in SHOD['parlist']:
      if(SHOD[pp][3]>0):
         pos0.append(SHOD[pp][0])
         print(pp,SHOD[pp][0],' free')
      else:
         print(pp,SHOD[pp][0],' fixed')

   pos0=np.array(pos0)

   #To count the number of minimizer step
   SHOD['minstepcount']=0

   res = minimize(lnprob_min, pos0, method='nelder-mead',#args=fchain,
           options={'xtol': 1e-3, 'disp': True, 'maxfev':1000000, 'maxiter':100000})
   print('Finished minmizing: %s'%GFQ.ltime())
   print(res)
   minresfile=SHOD['outroot']+'.minresult'
   write_minimizer_result(res,minresfile)

   print('Creating bestfit model and parameters')
   SHOD=BestFit(SHOD,res.x,tag='minimizer.bfit')
   sys.exit()
   return 0

#This function is called by minimization routines
def lnprob_min(theta):
    loglike=lnprob(theta,SHOD)
    if(loglike==-np.inf):
        chi2=1e10
    else:
        chi2=-2.0*loglike

    if(chi2<0):
        print("chi2=%f is less than zero!! This is from lnprob_min function"%chi2)
        fout_name=SHOD['outroot']+'.lnprob_min.failure'
        fout=open(fout_name,'w')
        fout.write('#Likelihood: %f\n'%(loglike))
        fout.write('#chi2: %f\n\n'%(chi2))
        #write out the dictionary
        print_dictionary(fout,SHOD,dicname='HOD',indent='')
        sys.exit(-1)

    #write the output
    ftmp=open(SHOD['minfile'],'a')
    ftmp.write('1.0 %12.8e'%loglike)
    for ii in range(0,theta.size):
       ftmp.write(' %12.8e'%theta[ii])
    ftmp.write('\n')
    ftmp.close()
    SHOD['minstepcount']=SHOD['minstepcount']+1

    if(SHOD['minstepcount']%100==0):
        print('minmizer step %d: %s chi2=%6.4f'%(SHOD['minstepcount'],GFQ.ltime(),chi2))
        sys.stdout.flush()
    return chi2


def test_likelihood(SHOD,nlike=4):
    #SHOD['nhocells']=(nlike*50)
    SHOD['nlike']=nlike
    fout_name=SHOD['outroot']+'.test.like.global.%d'%nlike
    fout=open(fout_name,'w')

    localDic=SHOD['pars'].copy() #copy the halo population stuff

    tm1=time.localtime()
    loglike=lnlike(SHOD,localDic)
    tm2=time.localtime()

    fout.write('####BEGIN  : '+str(tm1.tm_hour)+':'+str(tm1.tm_min)+':'+str(tm1.tm_sec)+'\n')
    fout.write('####END    : '+str(tm2.tm_hour)+':'+str(tm2.tm_min)+':'+str(tm2.tm_sec)+'\n')
    fout.write('#Likelihood: %f\n\n'%(loglike))

    #write out the dictionary
    print_dictionary(fout,SHOD,dicname='data',indent='')
    fout.close()

    print('check for test of global likelihood: ',fout_name)

    if(nlike>1):
       nlike=nlike-1
       test_likelihood(SHOD,nlike=nlike)
    else:
       sys.exit()
    return 0

# Define the probability function as likelihood * prior.
def map_param(theta,SHOD,LD):
   jj=0
   for ii,par in  enumerate(SHOD['parlist']):
      if(SHOD[par][3]<=0):
         LD[par]=SHOD[par][0]
      elif(SHOD[par][3]>0):
         LD[par]=theta[jj]
         #exec('classdata.argp.'+par+'='+str(theta(jj))   
         jj=jj+1

   return LD

def init_config(inifile=''):
   '''This function reads the initialization file and setup the mcmc'''

   #dictionary to store all the information
   SHOD={'parlist':['LMcut','sigmaM','gamma','fsat','fLRG','Mhknot'],
         'ndim':0}
   #for modeHM=4 parlist will be different
 
   lines=open(inifile,'r').readlines() #read all the lines

   #make sure the first line has the keyword
   if(lines[0][:-1]!='###init_mcm_QSOHOD###'):
      print('************************************************')
      print("The first line of config file doesn't match the requirement")
      print('it should be: ','###init_mcm_QSOHOD###')
      print('But it is :' ,lines[0][:-1])
      print('Quiting!!')
      print('************************************************')
      sys.exit(1)

   nline=len(lines)


   #list of strings to read from input file
   lstring=['outroot','HODdir','HODfile','modelHM','modelGAL','bfit',
            'cov_file','useQSO_frac','spline_version','nofsat','zspace','QSOsel']
   int_lstring=['bfit','modelHM','modelGAL','useQSO_frac','spline_version','nofsat']
   flt_lstring=[]
   
   #default values for missing inputs
   default_dic={'modelGAL':1,'useQSO_frac':0,'bfit':0, 'nofsat':0,
                'zspace':'real','QSOsel':None}

   SHOD['pars']={}

   #now go through all the lines and read the parameter information
   for tmp in lines:
      tsplit=tmp.split()
      if(tmp=='\n'):
         continue
      elif(tsplit[0] in lstring):
         if(tsplit[0] in int_lstring):
            SHOD[tsplit[0]]=np.int(tsplit[1])
         elif(tsplit[0] in flt_lstring):
            SHOD[tsplit[0]]=np.float(tsplit[1])
         else:
            SHOD[tsplit[0]]=tsplit[1]
      elif(tsplit[0]=='redshift'):
         SHOD[tsplit[0]]=np.float(tsplit[1])
      elif(tsplit[0]=='slim'):
         SHOD['slim']=[np.float(tsplit[1]),np.float(tsplit[2])]
      elif(tsplit[0] in SHOD['parlist']):
         SHOD[tsplit[0]]=np.array([np.float(tsplit[1]),np.float(tsplit[2]),np.float(tsplit[3]),np.float(tsplit[4])])
         SHOD['pars'][tsplit[0]]=SHOD[tsplit[0]][0]
         if(SHOD[tsplit[0]][3]>0):
            SHOD['ndim']=SHOD['ndim']+1

   #Any consistency check between input parameters should be done here
   #if(SHOD['modelHM']!=3 and SHOD['gamma'][3]>0):
   #    print('Quiting ---Inconsistent input!!')
   #    print('when model HM is not 3 then gamma must be fixed but it is not')
   #    sys.exit()

   #deal with command line modelGAl input
   if(args.modelGAL!=-1):
       SHOD['modelGAL']=args.modelGAL
       SHOD['outroot']=SHOD['outroot']+'-modelGAL-%d'%args.modelGAL
       print('Using modelGAL from command line:',SHOD['modelGAL'])


   for tt,tkey in enumerate(default_dic.keys()):
       if(tkey not in SHOD.keys()):
           SHOD[tkey]=default_dic[tkey]
           print(tkey,SHOD[tkey],' (using default value)')

   return SHOD

def lnprior(SHOD,LD):
   for pp in SHOD['parlist']:
      if(SHOD[pp][3]<0):
         continue
      elif(SHOD[pp][1]>LD[pp] or SHOD[pp][2]<LD[pp]):
         return -np.inf

   return 0.0

def lnprob(theta, SHOD):
    #To store all the things which gets updated every iteration for parallel mcmc consideration
    localDic=SHOD['pars'].copy() #copy the halo population stuff

    localDic=map_param(theta,SHOD,localDic)
    lp = lnprior(SHOD,localDic)
    if not np.isfinite(lp):
        return -np.inf
   # print(theta[0],SHOD['LM1'][0],lnlike(SHOD),lp)
    return lp + lnlike(SHOD,localDic)


def lnlike(SHOD,LD):
    '''Computes the lnlike'''  

    #compute model for the current set of parameters
    #step1: assign QSO
    #index_QSO,status=SHOD['QSO'].model_select_QSO_index(modelHM=SHOD['modelHM'],modelGAL=SHOD['modelGAL'],
    #        LMcut=LD['LMcut'],sigmaM=LD['sigmaM'],gamma=LD['gamma'],fsat=LD['fsat'])
    #print('in lnlike:LD: ',LD)
    if(SHOD['modelGAL']!=4):
        index_QSO,status=SHOD['QSO'].model_select_QSO_index(pdict=LD)
    else:
        index_QSO,status=SHOD['QSO'].model_select_QSO_index_GAL4(pdict=LD)
   

    if(status==0):#When not enough galaxy to have QSO we penalize model and go to else condition
        #step2 calculate auto and cross wp model
        wpmodel=SHOD['QSO'].wp_sample(index_QSO=index_QSO,autoQSO=1,crossELG=1,crossLRG=1,rsd=SHOD['zspace'],
                           multiprocess=1)

        indsel=SHOD['data']['indsel']
        #make a combined model vector of auto and corss
        model=np.append(wpmodel['QSO-auto'][indsel,1],wpmodel['LRG-cross'][indsel,1])
        model=np.append(model,wpmodel['ELG-cross'][indsel,1])

        #get the QSO fraction with density
        if(SHOD['useQSO_frac']==1):
            SHOD['QSO'].get_countgal_env(indQSO=index_QSO,env_list=[],werr=0)   
            mu_QSO=np.sum(SHOD['QSO'].countgal_dic['All'][:,0])/np.sum(SHOD['QSO'].countgal_dic['All'][:,1])
            #QSO fraction with overdensity
            QSO_frac=SHOD['QSO'].countgal_dic['All'][:,0]/SHOD['QSO'].countgal_dic['All'][:,1]
            #sscale the QSO fraction to match the mean fraction in data=0.029
            if(SHOD['QSOsel']==None):
                QSO_frac=0.029*QSO_frac/mu_QSO
            elif(SHOD['QSOsel']=='Lumtop50'):
                QSO_frac=0.01435*QSO_frac/mu_QSO
                
            model=np.append(model,QSO_frac) 

        diff=model-SHOD['data']['datavector']
        chi2=np.dot(diff,np.dot(SHOD['data']['icov'],diff))
    else:
        chi2=status

    loglike=-0.5*chi2

    return loglike

def initialize_par(SHOD,ndim,nwalkers):
   ''' This initialize without uniform distribution between the limits given in config file'''

   pos0 = [np.random.random(ndim) for i in range(nwalkers)]
   low=np.zeros(ndim)
   high=np.zeros(ndim)

   ii=0
   for pp in SHOD['parlist']:
      if(SHOD[pp][3]>0):
         low[ii]=SHOD[pp][1]
         high[ii]=SHOD[pp][2]
         ii=ii+1

   diff=high-low
   for ii in range(0,nwalkers):
      pos0[ii]=diff*pos0[ii]+low
      
   return pos0


def load_data(SHOD):

   #get the fitsfile name
   fname=SHOD['HODdir']+SHOD['HODfile']
   fin=F.FITS(fname)

   #create an object of model class #THis internally prepare everything for efficient calculation
   print('setting up QSO HOD object')
   if(SHOD['QSOsel']==None):
      nQSO=20000
   elif(SHOD['QSOsel']=='Lumtop50'):
      nQSO=10000
        
   SHOD['QSO']=MF.QSO_models_corr(fin,nLM=25,nQSO=nQSO,modelHM=SHOD['modelHM'],
                                modelGAL=SHOD['modelGAL'],nbin=5,rs=5,cat='mock',nofsat=SHOD['nofsat'],speedtest=0)

   if(SHOD['modelHM']==4):#The spline model is used so I should setup spline
      SHOD['QSO'].setup_spline(outroot=SHOD['outroot'],version=SHOD['spline_version'])
      #modify the parameter ranges
      Mhknot=np.copy(SHOD['Mhknot'])
      if(SHOD['QSO'].spline_version==1):
         #update the maximum such that normalization condition holds
         Mhknot[2]=1.0/len(SHOD['QSO'].knot_num)
         Mhknot[0]=0.5*Mhknot[2]
         Mhknot[3]=0.1*Mhknot[2]

      SHOD.pop('Mhknot')
      SHOD['pars'].pop('Mhknot')
      SHOD['ndim']=SHOD['ndim']-1
      print('updating parameter because spline is used')
      for ii in SHOD['QSO'].knot_num:
         par_this='Mhknot%d'%ii
         SHOD[par_this]=np.copy(Mhknot)
         SHOD[par_this][0]=np.random.random()*0.8+0.1
         SHOD['pars'][par_this]=Mhknot[0]
         SHOD['ndim']=SHOD['ndim']+1
         SHOD['parlist'].append(par_this)
         print(ii,par_this,SHOD[par_this])
      #new parameters
      SHOD['parlist']=SHOD['pars'].keys()
      print('updated parameters:',SHOD['parlist'])
      print('new dimension:',SHOD['ndim'])
      print('new SHOD[pars]=',SHOD['pars'])

      #check for consistency
      if(SHOD['nofsat']==1 and 'fsat' in SHOD['parlist']):
          print('*************************************')
          print('fsat is given while nofsat is 1')
          print('When nofsat is 1 then fsat is not used and must not be given for consistency')
          print('Exiting: Check the input file and try to re run')
          print('*************************************')
          sys.exit()

   #import pylab as pl
   #load the wp and covariances
   print('Loading wp data hard coded data files')
   SHOD['data']=GFQ.load_data_wp(z1=0.7,z2=1.1,useQSO_frac=SHOD['useQSO_frac']
                           ,rmin=SHOD['slim'][0],rmax=SHOD['slim'][1],QSOsel=SHOD['QSOsel'],plots=0)
   #pl.show()

   return SHOD


def chainsTostats(SHOD,chainfile):
   #load the chains file
   load_chain=np.loadtxt(chainfile)
   ####################################################

   #The last column is -logl ike, find mimimum to get best fit model
   indbfit=np.argmax(load_chain[:,1])
   #
   #Now write out some statistics
   outfile=SHOD['outroot']+'.stats'
   fout=open(outfile,'w')
   fout.write('#par mean std bfit(max -loglike) percentile(50, 16,84, 2.5,97.5,0.15,99.85)\n')
   jj=0
   for ii,par in enumerate(SHOD['parlist']):
      if(SHOD[par][3]<0): #the parameter is fixed
         continue

      labs='%10s %12.6f %12.6f %12.6f'%(par,np.mean(load_chain[:,jj+2]),
               np.std(load_chain[:,jj+2]),load_chain[indbfit,jj+2])
      tmpper=np.percentile(load_chain[:,jj+2],[50,16,84,2.5,97.5,0.15,99.85])
      for perc in tmpper:
         labs='%s %12.6f'%(labs,perc)
      fout.write(labs+'\n')
      jj=jj+1
   fout.close()
   print('written stats: ',outfile)

   mfitpars=np.mean(load_chain[:,2:],axis=0)
   bfitpars=load_chain[indbfit,2:]
   #####################################################
   return bfitpars,mfitpars


def BestFit(SHOD,bfitpars,tag=''):
   print('********************\nEvaluating Best fit results...',GFQ.ltime())
   if(SHOD['bfit']==0): #This means mcmc chains are ran
      SHOD['bfit']=1
   SHOD['tag']=tag

   LD=SHOD['pars'].copy()
   #wite the bestfit model data and error
   outfile=SHOD['outroot']+'.chi2'
   fout=open(outfile,'a')
   #maximum likelihood model
   jj=0
   for ii,pp in enumerate(SHOD['parlist']):
      if(SHOD[pp][3]>0):
         LD[pp]=bfitpars[jj]
         jj=jj+1

   loglike=lnlike(SHOD,LD)
   SHOD['chi2_Mfit']=-2.0*loglike
   dof=SHOD['data']['icov'].shape[0]-SHOD['ndim']+1
   fout.write('#chi2 %s fit model: %8.4f , dof= %d\n'%(tag,SHOD['chi2_Mfit'],dof))
   parstr='%s pars '%tag
   for tkey in SHOD['parlist']:
       parstr='%s %s = %12.6f '%(parstr,tkey,LD[tkey])
   fout.write(parstr+'\n')

   fout.close()
   print('written: ',outfile,GFQ.ltime())
   return SHOD

#This function is to create auxiliary files
def create_parfile(SHOD):
   #Initiate the files-names
   parfile=SHOD['outroot']+'.paramnames'
   rangefile=SHOD['outroot']+'.ranges'
   #initiate file pointer
   fpar=open(parfile,'w')
   frange=open(rangefile,'w')
   jj=0
   for pp in SHOD['parlist']:
      if(SHOD[pp][3]>=0):
         fpar.write('%s %s\n'%(pp,pp))
         frange.write('%20s %12.5e %12.5e\n'%(pp,SHOD[pp][1],SHOD[pp][2]))
      else:
         frange.write('%20s %12.5e %12.5e\n'%(pp,SHOD[pp][0],SHOD[pp][0]))

   #close the files
   fpar.close()
   print('Created par file file: ',parfile)
   frange.close()
   print('Created range file file: ',rangefile)
   return 0


if __name__=="__main__":
   print('current memory use in MB: ',get_memoryuse(py))
   print('time since begin: ',GFQ.timediff(time_beg,time.localtime()))
   #first create the mpi pool
   if(args.action==1):
      try:
         from emcee.utils import MPIPool
         pool = MPIPool(debug=False)
         use_mpi=1
      except:
         print('No mpi available not using mpi pool')
         use_mpi=0
   else:
       print('Not using mpi pool')
       use_mpi=0



   #Read config file
   print('reading config file:',args.config_file)
   SHOD=init_config(inifile=args.config_file)

   #if time needs to be stored
   if(args.storetime==1):
       file_time=SHOD['outroot']+'.time.memory'
       print('writing time and memory in file:',file_time)
       with open(file_time,'w') as ftime:
          ftime.write('#Time in sec, memomry in MB\n')
          ftime.write('%f %f\n'%(GFQ.timediff(time_beg,time.localtime()),get_memoryuse(py)))

   #load LF files
   SHOD=load_data(SHOD)
   

   #if action==2 then just test likelihood and exit
   if(args.action==2):
       print('Step3: Testing Likelihoods: %s'%GFQ.ltime())
       test_likelihood(SHOD,nlike=1)
       #test and exit otherwise just run the chains

   #if action ==4 then run scipy minimizer
   if(args.action==4):
       print('Step3: Running scipy minimzer: %s'%GFQ.ltime())
       run_minimizer(SHOD)


   #run mcmc only if bfit=0 otherwise directly load chains
   if(SHOD['bfit']==0):
      #now prepare for mcmc
      # Set up the sampler.
      ndim, nwalkers = SHOD['ndim'], args.nwalker
      #initialize the point
      pos0=initialize_par(SHOD,ndim,nwalkers)

      #initialize the sampler
      if(use_mpi==1):
          sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,args=[SHOD],pool=pool)
      else:
          sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,args=[SHOD])

      #####################################################
      # Clear and run the production chain.
      print("Started Running MCMC... %s"%GFQ.ltime())
      print("burnin: %d"%args.burnin)
      pos, prob, state =sampler.run_mcmc(pos0, args.burnin)
      sampler.reset()

      print("\n\nProduction MCMC... %s"%GFQ.ltime())

      #Initiate the chain files
      chainfile=SHOD['outroot']+'.txt'
      if(os.path.isfile(chainfile)):
         print('The chains file already exists (exiting): %s'%chainfile)
         sys.exit()
      else:
         #initiate chain file
         fchain=open(chainfile,'w')
         labs='#pars: weight, loglike '
         jj=0
         for pp in SHOD['parlist']:
            if(SHOD[pp][3]>=0):
               labs=labs+', '+pp
               jj=jj+1

         fchain.write(labs+'\n')
         fchain.close()
         print('Created Chain file: ',chainfile)
         #initiate parameter file
         create_parfile(SHOD)
    
      sys.stdout.flush()

      if(0): #simple run
         sampler.run_mcmc(pos, args.nsteps)# rstate0=np.random.get_state())
      if(1): #Advance run print progress bar
         width=0.01; fact=1.0/args.nsteps;
         wnow=0; nchain_pt=0
         for ss, result in enumerate(sampler.sample(pos, iterations=args.nsteps,storechain=False)):
            fchain=open(chainfile,'a')
            np.savetxt(fchain,np.column_stack([np.ones(nwalkers),result[1],result[0]]),fmt='%12.8e')
            nchain_pt=nchain_pt+nwalkers
            fchain.close()
            if(args.storetime==1):
               with open(file_time,'a') as ftime:
                  ftime.write('%f %f\n'%(GFQ.timediff(time_beg,time.localtime()),get_memoryuse(py)))

            if(fact*(ss+1)>= wnow):
               wnow=wnow+width
               print("{0:5.1%}".format(float(ss) / args.nsteps),GFQ.ltime(),nchain_pt)
               sys.stdout.flush()

      print("Finished MCMC saved chains in: %s  %s"%(chainfile,GFQ.ltime()))


      print("Mean acceptance fraction: {0:.3f}"
                      .format(np.mean(sampler.acceptance_fraction)))

   elif(SHOD['bfit']==1):
      #if only ran with bfit =1 then redefine chainfile
      chainfile=SHOD['outroot']+'.txt'


   #This runs after mcmc or for bfit!=0
   #write the stats
   bfitpars,mfitpars=chainsTostats(SHOD,chainfile)

   #write the bestfit HOD catalog and the correlation function
   SHOD=BestFit(SHOD,bfitpars,tag='bestfit')
   SHOD=BestFit(SHOD,mfitpars,tag='meanfit')


   if(use_mpi==1):
      # Close the processes.
      pool.close()



   #####################################################
   #Now make some plots
   #import corner
   #outfile=SHOD['outroot']+'_corner.png'
   #fig = corner.corner(samples, labels=SHOD['parlist'])
                               #truths=[m_true, b_true, np.log(f_true)])
   #fig.savefig(outfile)
   #print('saved: ',outfile)

   #####################################################
   #####################################################
