from __future__ import print_function
import numpy as np
import time

def ltime():
    tm=time.localtime()
    tmstr=str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec)
    return tmstr

def timediff(tm1,tm2):
    '''estimates the time difference in seconds'''
    dyear=tm2.tm_year-tm1.tm_year
    dday=tm2.tm_yday-tm1.tm_yday
    dhour=tm2.tm_hour-tm1.tm_hour
    dmin=tm2.tm_min-tm1.tm_min
    dsec=tm2.tm_sec-tm1.tm_sec
   
    tdiff=(((dyear*365+dday)*24.+dhour)*60+dmin)*60+dsec
    return tdiff


def wp_withQSO_selection(sample='QSO-auto'):
    
    wpdir='/disk2/salam/eBOSS/ELG/ELGv4/XI/WP/'
    if(sample=='QSO-auto'):
        fname='DR16QSO-comb-N-S1-S2-sel-iPSFMAG-0-50-All-colsel-full-'
        fname=fname+'wtag-wsys-rfact10-bin5-wp-zlim-0.70-1.10-nmerge-6-wp-logrp-pi-NJN-37.txt'
    elif(sample=='LRG-cross'):
        fname='DR16LRG-DR16QSO-comb-N-N-S1-S1-S2-S2-sel-crossAll_iPSFMAG-0-50-All-colsel-full-'
        fname=fname+'wtag-wsys-rfact10-bin5-wp-zlim-0.70-1.10-wp-logrp-pi-NJN-222.txt'
    elif(sample=='ELG-cross'):
        fname='ELGv4-DR16QSO-comb-eboss21-S2-eboss22-S2-eboss23-N-eboss25-N-sel-crossAll_iPSFMAG-0-50-All'
        fname=fname+'-colsel-full-wtag-wsys-rfact10-bin5-wp-zlim-0.70-1.10-wp-logrp-pi-NJN-92.txt'
        
    return fname


def wp_file_public(sample='QSO-auto',QSOsel=None):
    '''returns the file paths for the clustering measurements'''
    wpdir='../data/'

    if(QSOsel==None):
        seltag=''
    else:
        seltag='-Lumtop50'

    if(sample=='QSO-auto'):
        fname='%sQSO-auto%s.wp'%(wpdir,seltag)
    elif(sample=='LRG-cross'):
        fname='%sQSOxLRG-cross%s.wp'%(wpdir,seltag)
    elif(sample=='ELG-cross'):
        fname='%sQSOxELG-cross%s.wp'%(wpdir,seltag)
    elif(sample=='QSO-delta'):
        fname='%sfQSO-delta%s.txt'%(wpdir,seltag)

    return fname

#This is outside the class
def load_data_wp(z1=0.7,z2=1.1,useQSO_frac=0,rmin=10,rmax=40,QSOsel=None,plots=0):
    '''This loads the auto and cross-correlation from data
    QSOsel=None full sample
    QSOsel=Lumtop50 50% brightest sample'''

    #select redshift range
    ztag='zlim-%4.2f-%4.2f'%(z1,z2)

    wpdata={}

    wpfname={'QSO-auto':wp_file_public(sample='QSO-auto',QSOsel=QSOsel),
            'LRG-cross':wp_file_public(sample='LRG-cross',QSOsel=QSOsel),
            'ELG-cross':wp_file_public(sample='ELG-cross',QSOsel=QSOsel),
            }

    for tt,tkey in enumerate(['QSO-auto','LRG-cross','ELG-cross']):
        fname=wpfname[tkey]
        wpdata[tkey]=np.loadtxt(fname)

    #collect data 
    ind1=wpdata[tkey][:,0]>rmin
    ind2=wpdata[tkey][:,0]<rmax
    wpdata['indsel']=ind1*ind2
    wpdata['datavector']=np.append(wpdata['QSO-auto'][wpdata['indsel'],1],wpdata['LRG-cross'][wpdata['indsel'],1])
    wpdata['datavector']=np.append(wpdata['datavector'],wpdata['ELG-cross'][wpdata['indsel'],1])

    quants=['QSO-auto','LRG-cross','ELG-cross']

    if(useQSO_frac==1):
        #use QSO fraction in the contraint here we load the measurements
        nbin=5 #using 5 bins in delta for fitting
        fname=wp_file_public(sample='QSO-delta',QSOsel=QSOsel)
        wpdata['QSOdel']=np.loadtxt(fname)
        wpdata['datavector']=np.append(wpdata['datavector'],wpdata['QSOdel'][:,1])

        quants.append('QSOdel')


    ndata=wpdata['datavector'].size
    #full ocvariance matrix
    wpcov_full=np.zeros(ndata*ndata).reshape(ndata,ndata)

    #Get covariances
    wpcov={};endi=0 
    for tt,tkey in enumerate(quants): 
        if(tkey!='QSOdel'):
            indsel=wpdata['indsel']
        else:
            indsel=np.ones(wpdata[tkey].shape[0],dtype=bool)

        wpcov[tkey]=np.cov(wpdata[tkey][indsel,4:])*(wpdata[tkey].shape[1]-4-1)
        #a factor of 2 in covariance is needed to account for equaltheory error
        if(tkey=='QSO-auto'):
           print(tkey,'Note a factor of 2 is used in covariance to account for theory error for ',tkey)
           wpcov[tkey]=wpcov[tkey]*2.0

        nrp=np.sum(indsel)
        begi=endi; endi=begi+nrp
        print(tt,tkey,begi,endi,nrp)
        wpcov_full[begi:endi,begi:endi]=wpcov[tkey]

    #get the inverse covariance matrix
    wpdata['icov']=np.linalg.inv(wpcov_full)

    if(plots==1):
        import pylab as pl
        nrow=4;ncol=3
        fig ,axarr=pl.subplots(nrow,ncol,sharex=False,sharey=False,figsize=(15,nrow*5))
        axarr=axarr.reshape(axarr.size)

        nplot=0
        for tt,tkey in enumerate(quants):
            pl.sca(axarr[nplot])
            pl.errorbar(wpdata[tkey][:,0],wpdata[tkey][:,1],yerr=wpdata[tkey][:,2],label=tkey)
            pl.title(tkey);pl.legend()
            if(tkey!='QSOdel'):
               pl.xscale('log');pl.yscale('log');

            nplot=nplot+1
            #plot the correlation matrices
            pl.sca(axarr[nplot])
            wpcorr=np.copy(wpcov[tkey])
            for ii in range(0,wpcov[tkey].shape[0]):
                for jj in range(0,wpcov[tkey].shape[0]):
                    wpcorr[ii,jj]=wpcov[tkey][ii,jj]/np.sqrt(wpcov[tkey][ii,ii]*wpcov[tkey][jj,jj])
            pl.imshow(wpcorr)
            pl.title(tkey)
            nplot=nplot+1

        #plot the full correlation matrix

        wpcorr=np.copy(wpcov_full)
        wpicorr=np.copy(wpdata['icov'])
        for ii in range(0,wpcov_full.shape[0]):
            for jj in range(0,wpcov_full.shape[0]):
                wpcorr[ii,jj]=wpcov_full[ii,jj]/np.sqrt(wpcov_full[ii,ii]*wpcov_full[jj,jj])
                wpicorr[ii,jj]=wpdata['icov'][ii,jj]/np.sqrt(wpdata['icov'][ii,ii]*wpdata['icov'][jj,jj])

        pl.sca(axarr[nplot]);nplot=nplot+1
        pl.imshow(wpcorr);pl.colorbar();pl.title('full correlation matrix')
        pl.sca(axarr[nplot]);nplot=nplot+1
        pl.imshow(wpicorr);pl.colorbar();pl.title('full inverse correlation matrix')


        pl.sca(axarr[nplot])
        pl.plot(wpdata['datavector'],'o')
        pl.title('selected data vector')

        #show this plot
        pl.show()

    return wpdata
