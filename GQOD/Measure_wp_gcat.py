#This simply measure the wp of a galaxy catalog
from __future__ import print_function


import numpy as np
import sys
#sys.path.insert(0,'../MultiHODFitter/')
#import emcee_MultiHOD_sampler as eMHODsamp

#sys.path.insert(0,'../HODFitter/')
#import General_function_HOD as GFH

from Corrfunc.theory import DDrppi as MdeepDD


import argparse



def Analytic_volume(rpers,pimax,dpi=1,ngal=1,boxsize=1):
    #estimate the volume or RR count analytically for periodic box
    rmax=rpers[1:]; rmin=rpers[:-1]
    RR_x=np.pi*(np.power(rmax,2)-np.power(rmin,2))*dpi*np.power(ngal,2)/np.power(boxsize,3)
    npi=np.int(pimax)
    RR_xy=np.zeros(RR_x.size*npi).reshape(RR_x.size,npi)
    for ii in range(0,npi):
       RR_xy[:,ii]=RR_x
    return RR_xy


def get_wp(SHOD,LD,data2='',xitype='auto'):

    rlim=np.array([SHOD['wp_rper'][0],SHOD['wp_rper'][1],
             SHOD['wp_rpi'][0],SHOD['wp_rpi'][1]],dtype='double')
    SHOD['rpers']=np.linspace(rlim[0],rlim[1],SHOD['wp_rper'][2]+1)
    SHOD['rpers']=np.power(10,SHOD['rpers']) #take antilog needed for Manodeep xi code
    SHOD['rpers_mid']=0.5*(SHOD['rpers'][1:]+SHOD['rpers'][:-1])
    #SHOD['rpars']=np.linspace(rlim[2],rlim[3],np.int(2*rlim[3]+1))
    #SHOD['rpars_mid']=0.5*(SHOD['rpars'][1:]+SHOD['rpars'][:-1])
    SHOD['pimax']=rlim[3]

    #compute the volume factor RR count for correlaton function
    SHOD['RR_xy']=Analytic_volume(SHOD['rpers'],SHOD['pimax'],
            dpi=2,ngal=1,boxsize=SHOD['Lbox'])

    if(xitype=='auto'):
       DD_result= MdeepDD(1,SHOD['nthreads'], SHOD['pimax'], SHOD['rpers'],
       np.float32(LD['gcat'][:,0]), np.float32(LD['gcat'][:,1]),
       np.float32(LD['gcat'][:,2]), weights1=None, periodic=True,
       X2=None, Y2=None, Z2=None, weights2=None, verbose=False, boxsize=SHOD['Lbox'],
       output_rpavg=False, xbin_refine_factor=2, ybin_refine_factor=2,
       zbin_refine_factor=1, max_cells_per_dim=100,
       c_api_timer=False, isa=r'fastest', weight_type=None)
       #random random count
       RRcount=np.power(LD['gcat'].shape[0],2)*SHOD['RR_xy']
    elif(xitype=='cross'):
       DD_result= MdeepDD(0,SHOD['nthreads'], SHOD['pimax'], SHOD['rpers'],
       np.float32(LD['gcat'][:,0]), np.float32(LD['gcat'][:,1]),
       np.float32(LD['gcat'][:,2]), weights1=None, periodic=True,
       X2=np.float32(data2[:,0]), Y2=np.float32(data2[:,1]), Z2=np.float32(data2[:,2]), 
       weights2=None, verbose=False, 
       boxsize=SHOD['Lbox'],
       output_rpavg=False, xbin_refine_factor=2, ybin_refine_factor=2,
       zbin_refine_factor=1, max_cells_per_dim=100,
       c_api_timer=False, isa=r'fastest', weight_type=None)
       #random random count
       RRcount=LD['gcat'].shape[0]*data2.shape[0]*SHOD['RR_xy']

    #estimate wp_xy
    wp_xy=DD_result['npairs'].reshape(SHOD['rpers_mid'].size,np.int(SHOD['pimax']))
    wp_xy=(wp_xy/RRcount)-1.0
    #projected correlation function
    wp1d=2.0*np.sum(wp_xy,axis=1)

    LD['model']=np.column_stack([SHOD['rpers_mid'],wp1d])

    return SHOD,LD

def load_catalog(LD,SHOD,cat,cat_type,rsd,tkey='gcat',ncat=1,subsamp=1.0):
   #load the galaxy catalog
   if(cat_type==''):
      LD[tkey]=np.loadtxt(cat)
   elif('MultiHOD' in cat_type): #MultiHOD file and select LRG
      tdata=np.loadtxt(cat)
      if('LRG' in cat_type):
         ind=tdata[:,-1]==1
      elif('QSO' in cat_type):
         ind=tdata[:,-1]==2
      elif('ELG' in cat_type):
         ind=tdata[:,-1]==3
      else:
         ind=np.ones(tdata.shape[0],dtype=bool)

      LD[tkey]=tdata[ind,:]

   elif(cat_type=='outerrim'):
      for ii in range(0,ncat):
         fname='%s%s.gcat'%(cat,str(ii))
         tdata=np.loadtxt(fname)
         if(subsamp<1):
             nsub=np.int(tdata.shape[0]*subsamp)
             ind=np.random.choice(np.arange(0,tdata.shape[0]),size=nsub,replace=False)
         else:
             ind=np.arange(0,tdata.shape[0])

         print('loaded : %d/%d %s'%(ind.size,tdata.shape[0],fname))
         if(tkey not in LD.keys()):
            LD[tkey]=tdata[ind,:6]
         else:
            LD[tkey]=np.row_stack([LD[tkey],tdata[ind,:6]])
   elif(cat_type=='particle'):
      for ii in range(0,64):
         fname='%s%s'%(cat,str(ii).zfill(2))
         tdata=np.loadtxt(fname)
         print('loaded :',fname)
         if(tkey not in LD.keys()):
            LD[tkey]=tdata[:,1:4]
         else:
            LD[tkey]=np.row_stack([LD[tkey],tdata[:,1:4]])

   LD['Ngal']=LD[tkey].shape[0]

   #apply RSD
   #applying the redshift space distortion
   if(rsd!='real'):
       Hz=100.0 # so that we get result in Mpc/h, velocities assumed in km/s
       if(rsd=='zRSD'):
            LD[tkey][:,2]=LD[tkey][:,2]+(LD[tkey][:,5]*(1+SHOD['redshift'])/Hz)
            #apply modulo box size
            LD[tkey][:,2]=np.mod(LD[tkey][:,2],SHOD['Lbox'])
       elif(rsd=='1pzOEZ_zRSD'):
            omM=0.31 #cosmology for MDPL2
            Ez=np.sqrt((np.power(1+SHOD['redshift'],3)*omM)+(1-omM))
            zsim_factor=(1+SHOD['redshift'])/(100.0*Ez)
            LD[tkey][:,2]=LD[tkey][:,2]+(LD[tkey][:,5]*zsim_factor)
            #apply modulo box size
            LD[tkey][:,2]=np.mod(LD[tkey][:,2],SHOD['Lbox'])
       elif(args.rsd=='yRSD'):
            rsdpos=LD[tkey][:,1]+(LD[tkey][:,4]*(1+SHOD['redshift'])/Hz)
            #apply modulo box size
            rsdpos=np.mod(rsdpos,SHOD['Lbox'])
            LD[tkey][:,1]=np.copy(LD[tkey][:,2])
            LD[tkey][:,2]=rsdpos
       elif(args.rsd=='xRSD'):
            rsdpos=LD[tkey][:,0]+(LD[tkey][:,3]*(1+SHOD['redshift'])/Hz)
            #apply modulo box size
            rsdpos=np.mod(rsdpos,SHOD['Lbox'])
            LD[tkey][:,0]=np.copy(LD[tkey][:,2])
            LD[tkey][:,2]=rsdpos

   LD['alpha_per']=1.0

   return LD



if __name__=="__main__":
   parser = argparse.ArgumentParser(description='takes the galaxy catalogue and measures wp uses a config file same as HOD one to get the binning and other details')
   #these should be input
   parser.add_argument('-Lbox'  ,type=float,default=1000)
   parser.add_argument('-redshift'  ,type=float,default=0)
   parser.add_argument('-pimax'  ,type=float,default=40,help='line of sight integration')
   parser.add_argument('-logrper' ,nargs='+',type=float,default=[-2,1.477,25],
           help='''This is log binning of rper''')

   parser.add_argument('-subsamp'  ,type=float,default=1.0,help='set this to sme value to subsample the catalogues')

   parser.add_argument('-cat',default='/disk2/salam/eBOSS_HOD_chains/MDPL2_HOD_cat/model1.gcat',
           help='galaxy catalogue with columns as x,y,z,vx,vy,vz')
   parser.add_argument('-ncat'  ,type=int,default=1,help='number of catalogues in case more than one')
   parser.add_argument('-rsd',default='real',help='real for no rsd or xRSD,yRSD,or zRSD for x,y or zRSD')
   parser.add_argument('-cat_type',default='',help='''particle: read thorugh multiple file with first column as particle id, 
                                            outerrim: to read outerrim HOD
                                            MultiHOD-LRG, MultiHOD-QSO, MultiHOD-ELG select assuming the last column in the file
                                            have LRG=1, QSO=2 and ELG=3''')
   parser.add_argument('-outroot',default='',help='if empty then cat is used to write output file otherwise this is used')

   #second catalog
   parser.add_argument('-cat2',default='',
           help='galaxy catalogue with columns as x,y,z,vx,vy,vz')
   parser.add_argument('-rsd2',default='real',help='real for no rsd or xRSD,yRSD,or zRSD for x,y or zRSD')
   parser.add_argument('-cat2_type',default='',help='particle: read thorugh multiple file with first column as particle id')
   parser.add_argument('-ncat2'  ,type=int,default=1,help='number of catalogues in case more than one')

   args = parser.parse_args()

   #config_file=args.config_file
   #SHOD=eMHODsamp.init_config(inifile=config_file)
   SHOD={'redshift':args.redshift,'Lbox':args.Lbox,
           'wp_rper':args.logrper,'wp_rpi':[-args.pimax,args.pimax],
           'nthreads':4}

   #hold it in a dictionary
   LD={}
   LD=load_catalog(LD,SHOD,args.cat,args.cat_type,args.rsd,tkey='gcat',ncat=args.ncat,subsamp=args.subsamp)
   print('Total number of galaxies:',LD['Ngal'])
   if(args.cat2!=''):
      LD=load_catalog(LD,SHOD,args.cat2,args.cat2_type,args.rsd2,tkey='gcat2',ncat=args.ncat2,subsamp=args.subsamp)
      print('Total number of galaxies (second sample):',LD['Ngal'])


   if(args.outroot==''):
      outroot=args.cat
   else:
      outroot=args.outroot

   #old method
   #if(0):
      #SHOD,LD=GFH.get_HOD_xistat(SHOD,LD)
      #SHOD,LD=GFH.get_HOD_xistatdeep(SHOD,LD)
      #outname='%s.wp.%s'%(outroot,args.rsd)

   #calculate auto or cross correlation
   if(args.cat2!=''):#compute the cross correlation
      #new function
      SHOD,LD=get_wp(SHOD,LD,data2=LD['gcat2'],xitype='cross')
   else: #auto correlation
      SHOD,LD=get_wp(SHOD,LD,data2='',xitype='auto')

   outname='%s.wp.%s'%(outroot,args.rsd)
   #To save the model
   np.savetxt(outname,LD['model'],header=' %s\n rp (Mpc/h) wp'%args.cat)
   print('saved: %s'%outname)




