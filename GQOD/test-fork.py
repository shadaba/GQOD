# Python code to create child process 
import os 
import time


class ParentChild :
    def __init__(self):
       self.setup=1

    def compute(self,child=0):
       #for parent just wait
       #if(child!=0):
       #   self.status=os.waitpid(os.getpid(),0)
       #for child compute
       if(child==0):
          self.child_res=10
          time.sleep(1)
          exit()
       

    def getresult(self):
       return self.child_res
       

def parent_child(): 
	child_pid = os.fork() 
        thisval=0
	# n greater than 0 means parent process 
	if child_pid > 0:
                print os.waitpid(child_pid, 0)
                while(False):
                    time.sleep(0.5)
                    print('parent',thisval)
                    if(thisval>=5):
                        break
		print("Parent process and id is : ", os.getpid(),thisval) 

	# n equals to 0 means child process 
	else: 
		print("Child process and id is : ", os.getpid()) 
                for ii in range(0,5):
                   time.sleep(1)
                   thisval=thisval+1
                   print('child',ii,thisval)
        thisval=100
		
# Driver code 
#parent_child() 


if(1):
   cobj=ParentChild()
   child_pid=os.fork()
   if(child_pid!=0):
      #cobj.compute(child=child_pid)
      cobj.status=os.waitpid(child_pid,0)
   else:
      cobj.compute(child=child_pid)

   print(cobj.getresult())
   print('status: ',cobj.status)
