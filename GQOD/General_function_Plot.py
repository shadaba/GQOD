from __future__ import print_function

import sklearn; print("  scikit-learn:", sklearn.__version__)
from sklearn.neighbors import KernelDensity
import numpy as np
import os
import sys

#from scipy.interpolate import interp1d
#from scipy import optimize


#cosmological calculation
sys.path.insert(0,'/disk1/salam/Projects/shadab_pythonlib/')
import mycosmo as mcosm

#sys.path.insert(0,'/disk1/salam/Projects/HODFitter/')
#import HOD_populate as hpop

####plottinng library and setting
import pylab as pl
import matplotlib
matplotlib.rcParams.update({'font.size': 18, 'font.family': 'serif'})


def load_chains(chainroot='',ext='',nchain=1,burnin=0):
    mapdict={}
    pfile=chainroot+'.paramnames'+ext
    
    
    #read the parameters from file
    lines=open(pfile).readlines()
    for ll,tline in enumerate(lines):
        parts=tline.split()
        mapdict[parts[0]]=ll+2
        
    #make the maps of column to parameter name
    
    #load the chain
    if(nchain==1):
        chainfile=chainroot+'.txt'+ext
    else:
        chainfile=chainroot+'_1.txt'
        
    Lchain=np.loadtxt(chainfile)
    nsamp=Lchain.shape[0]
    nsel=np.int(burnin*nsamp)
    Lchain=Lchain[nsel:,:]
    
    if(nchain>1):
        for ii in range(2,nchain+1):
            chainfile=chainroot+'_%d.txt'%ii
            tmpchain=np.loadtxt(chainfile)
            nsamp=tmpchain.shape[0]
            nsel=np.int(burnin*nsamp)
            
            Lchain=np.row_stack([Lchain,tmpchain[nsel:,:]])
            

    return Lchain,mapdict



def kde_sklearn_2d(x, x_grid, bandwidth=0.2, **kwargs):
    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x)
    log_pdf = kde_skl.score_samples(x_grid)
    return np.exp(log_pdf)

def kde_sklearn_1d(x, x_grid, bandwidth=0.2, **kwargs):
    """Kernel Density Estimation with Scikit-learn"""
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)

def sigma2d_2_prob(sigin=1):
    '''This gives the value of volume to sigma relation based on this:
    https://corner.readthedocs.io/en/latest/pages/sigmas.html'''
    val=1.0 - np.exp(-np.power(sigin,2)/2.0)
    return val

def plot_2dcontours(xysample='',nx=100,ny=100,sigmas=np.array([1,2,3]),ctype='line', bandwidth=0.2,
                    xlims=[],ylims=[],colorin='r',labelin='',lwin=2,outroot='',plots=1):
    '''This plots the 2d cotnours at 
    sigmas: decide what levels to plot should be sorted
    ctype:  "line" or "filled" to decide between line or filled cotour
    if outroot is not empty then it will first try to load the density from file
    otherwise will save the density to a file once calculated
    when outroot is provided then xysample is not used'''
    
    rho2d=''
    
    if(outroot!=''):
        outfname='%s-nx-%d-ny-%d_2d.txt'%(outroot,nx,ny)
        if(os.path.isfile(outfname)):
            rho2d=np.loadtxt(outfname)
            x2d_grid=rho2d[:,0].reshape(nx,ny)
            y2d_grid=rho2d[:,1].reshape(nx,ny)
            pdf=rho2d[:,2]
            #if(plots!=1):
            #   print('reading: ',outfname)
     
    if(rho2d==''): #compute density and save it 
        #first creates the sampling in 1d parameters
        x_grid=np.linspace(xlims[0],xlims[1],nx+1)
        y_grid=np.linspace(ylims[0],ylims[1],ny+1)
        x_mid=0.5*(x_grid[1:]+x_grid[:-1])
        y_mid=0.5*(y_grid[1:]+y_grid[:-1])

        #create 2d grid for density estimates
        x2d_grid,y2d_grid=np.meshgrid(x_mid,y_mid,indexing='ij')

        xy_grid=np.column_stack([x2d_grid.reshape(x2d_grid.size),y2d_grid.reshape(y2d_grid.size)])

        #estimate the density
        if(0): #kde density estimated
           pdf = kde_sklearn_2d(xysample, xy_grid, bandwidth=bandwidth)
           pdf=pdf/np.sum(pdf) #normalizing
        else: #histogram and smoothing
           pdf=rho2d_hist(xysample,x_grid,y_grid,np.int(1.0/bandwidth))
        np.savetxt(outfname,np.column_stack([xy_grid,pdf]))
        #if(plots!=1):
        #   print('written: ',outfname)
    
    if(plots==1):
        #Now figure out sigma levels
        tpdf=np.sort(pdf)
        ctpdf=np.cumsum(tpdf[::-1])
        ctpdf=ctpdf/ctpdf[-1]


        #sort sigmas
        sigmas=np.sort(sigmas)

        clev=[]; lstyles=['dotted','dashed','solid']
        for ss, sig in enumerate(np.sort(sigmas)[::-1]):
            this_val=sigma2d_2_prob(sigin=sig)
            indmin=np.argmin(np.abs(ctpdf-this_val))
            clev.append(tpdf[-indmin])



        #Now prepare for plotting
        pdf=pdf.reshape(nx,ny)
        if(ctype=='line'):
            pl.contour(x2d_grid,y2d_grid, pdf,colors=colorin,levels=clev,
                   linestyles=lstyles[-len(clev):],label=labelin,linewidths=lwin)
        elif(ctype=='filled'):
            clev.append(np.max(pdf)); nc=len(clev)
            alphas=[0.5,0.3,0.1]
            for aa, alpha in enumerate(alphas[:nc-1]):
                if(aa==0):
                    tlab=labelin
                else:
                    tlab=''

                pl.contourf(x2d_grid,y2d_grid, pdf,colors=colorin,levels=clev[-aa+nc-2:-aa+nc],alpha=alpha,label=tlab)
    return

def rho2d_hist(xysample,xbin,ybin,box_pts):
    '''Computes the 2d histogram and smooth it with box_pts on each axis'''

    H,xed,yed=np.histogram2d(xysample[:,0],xysample[:,1],bins=[xbin,ybin],normed=1)

    #smooth along x-axis and y-axis
    for ii in range(0,H.shape[0]):
        H[ii,:]=smooth1d(H[ii,:], box_pts)
    for jj in range(0,H.shape[1]):
        H[:,jj]=smooth1d(H[:,jj], box_pts)

    H=H.reshape(H.size)
    return H

#def smooth1d(y, box_pts):
#    box = np.ones(box_pts)/box_pts
#    y_smooth = np.convolve(y, box, mode='same')
#    return y_smooth

def smooth1d(yy, box_pts):
    
    if(box_pts%2==1): #odd number of points for smoothing
        nbeg_valid=np.int(0+0.5*(box_pts-1))
        nend_valid=np.int(yy.size-0.5*(box_pts-1))
    else:
        nbeg_valid=np.int(0+0.5*box_pts-1)
        nend_valid=np.int(yy.size-0.5*box_pts)
    
    #print(nbeg_valid,nend_valid,nend_valid-nbeg_valid)
    #smoothing window
    box = np.ones(box_pts)/box_pts
    
    #convoled for valid points
    y_smooth=np.zeros(yy.size)
    
    y_smooth[nbeg_valid:nend_valid] = np.convolve(yy, box, mode='valid') 
    
    
    #smooth the boundary points with different smoothing length
    for ii in range(0,nbeg_valid):
        bw=ii
        y_smooth[ii]=np.mean(yy[ii-bw:ii+bw+1])
        
    for ii in range(nend_valid,yy.size):
        bw=yy.size-ii-1
        y_smooth[ii]=np.mean(yy[ii-bw:ii+bw+1])  
    
    return y_smooth



def plot_1dlike(xsample='',nx=100, bandwidth=0.2,xlims=[],lwin=2,
        colorin='r',labelin='',outroot='',ls='-',plots=1):
    '''This plots the 2d cotnours at 
    sigmas: decide what levels to plot should be sorted
    if outroot is not empty then it will first try to load the density from file
    otherwise will save the density to a file once calculated'''
    
    rho1d=''
    
    if(outroot!=''):
        outfname='%s-nx-%d_1d.txt'%(outroot,nx)
        if(os.path.isfile(outfname)):
            rho1d=np.loadtxt(outfname)
            x1d_grid=rho1d[:,0]
            pdf=rho1d[:,1]
            #if(plots!=1):
            #   print('reading: ',outfname)
        else:
            print(outfname,' not found')
     
    res_stat={}
    if(rho1d==''): #compute density and save it 
        #first creates the sampling in 1d parameters
        #x1d_grid=np.linspace(xsample.min(),xsample.max(),nx)
        if(0): #kde density estimate seems to be biased
           x1d_grid=np.linspace(xlims[0],xlims[1],nx)
           #estimate the density
           pdf = kde_sklearn_1d(xsample, x1d_grid, bandwidth=0.2)
           pdf=pdf/np.sum(pdf) #normalizing
        else: #histogram density estimate
           xed=np.linspace(xlims[0],xlims[1],nx)
           x1d_grid=0.5*(xed[1:]+xed[:-1])
           pdf,hc=np.histogram(xsample,bins=xed,normed=1)
           res_stat=compute_stats(xsample,xed,pdf,outfname=outfname+'.stats') #This must be called before smoothing the pdf
           pdf=smooth1d(pdf, np.int(1.0/bandwidth))

        np.savetxt(outfname,np.column_stack([x1d_grid,pdf]))
        #if(plots!=1):
        #   print('written: ',outfname)

    
    if(plots==1):
        pl.plot(x1d_grid,pdf,color=colorin,ls=ls,lw=lwin,label=labelin)

    return res_stat


def compute_stats(xsample,xed,pdf,outfname=''):
    '''This computes several stats for a marginalized likelihood
    Note that generally 1sigma and 2 sigma are of good accuracy but 3 sigma is biased low by 3%
    as per the tests on gaussian'''
    gauss_percentile={'1s':0.341,'2s':0.341+0.136,'3s':0.341+0.136+0.021}

    #dictionary to hold results
    res_stat={'mu':np.mean(xsample),'std':np.std(xsample)}
    #calculate the most likely point and the 1sigma,2sigma,3sigma
    indmax=np.argmax(pdf)
    xed_mid=0.5*(xed[1:]+xed[:-1])
    res_stat['maxlike']=xed_mid[indmax]
    #Calulcate the total area
    total_area=np.sum(pdf)
    area_cum=1.0*np.cumsum(pdf)/total_area
   
    #interpolate the area with parameter value 
    #farea = interp1d(xed[1:],area_cum, kind='cubic')

    for ss,skey in enumerate(['1s','2s']):
       #upside
       #interp_fn_up = lambda x: farea(x) - (0.5+gauss_percentile[skey])
       #res_stat['%s_up'%skey]=optimize.newton(interp_fn_up, xed[indmax])
       #downside
       #interp_fn_dn = lambda x: farea(x) - (0.5-gauss_percentile[skey])
       #res_stat['%s_dn'%skey]=optimize.newton(interp_fn_dn, xed[indmax])

       res_stat['%s_up'%skey]=interpolate(xed[1:],area_cum,0.5+gauss_percentile[skey])
       res_stat['%s_dn'%skey]=interpolate(xed[1:],area_cum,0.5-gauss_percentile[skey])
    #median
    #interp_fn = lambda x: farea(x) - 0.5
    #res_stat['median']=optimize.newton(interp_fn, xed[indmax])
    res_stat['median']=interpolate(xed[1:],area_cum,0.5)

    if(outfname!=''): #write the stats in an output file
        fout=open(outfname,'w')
        for skey in sorted(res_stat.keys()):
           fout.write('%s %g\n'%(skey,res_stat[skey]))
        fout.close()

    return res_stat

def interpolate(xvals,area_cum,expected_value):
    '''This compute the value at the expected area under the curve byt interpolation'''
    #find the nearest point
    ind_near=np.argmin(np.abs(area_cum-expected_value))
    
    if(area_cum[ind_near]<expected_value):
        #interpolate linearly
        slope=(area_cum[ind_near+1]-area_cum[ind_near])/(xvals[ind_near+1]-xvals[ind_near])
        intercept=area_cum[ind_near]-slope*xvals[ind_near]
        res=(expected_value-intercept)/slope
    else:
        slope=(area_cum[ind_near-1]-area_cum[ind_near])/(xvals[ind_near-1]-xvals[ind_near])
        intercept=area_cum[ind_near]-slope*xvals[ind_near]
        res=(expected_value-intercept)/slope
        
    return res

def Get_par_prior(parin=''):
    par_prior={'LMcut':[12.0,15.0],'LM1':[12.0,15.0],
            'sigma':[0.1,8],'alpha':[0.1,3.0],'kappa':[0.1,3.0],
            'gammaHV':[0.1,3.0],'gammaIHV':[0.1,3.0],
            'fconc':[0.2,8.0],'alpha_par':[0.5,1.5],'alpha_per':[0.5,1.5],
            'Ngal*':[0,1e6],'ssp':[0.5,2.0],
            'fsat':[0,1],'fLRG':[0,1],
            'Mhknot0':[0,1],'Mhknot1':[0,1],'Mhknot2':[0,1],'Mhknot3':[0,1],'Mhknot4':[0,1],
            'Mhknot5':[0,1],'Mhknot6':[0,1],'Mhknot7':[0,1],'Mhknot8':[0,1],}
    return par_prior[parin]


def getNsat(tchain,tdict,Mhalo,meanBCG):
    '''assumes that Mhalo is a scalar'''
    
    Mdiff=(Mhalo-tchain[:,tdict['kappa']]*np.power(10,tchain[:,tdict['LMcut']]))
    Mdiff=Mdiff/np.power(10,tchain[:,tdict['LM1']])
    
    Nsat=np.zeros(Mdiff.size)
    index=Mdiff>0
    Nsat[index]=meanBCG[index]*np.power(Mdiff[index],tchain[index,tdict['alpha']])

    return Nsat

def HOD_occupation(tchain,tdict,plots=0,outwrite=''):
    #MDPLe halo mass function
    #MDPL2_hmf_file='/disk1/salam/Projects/HODFitter/MDPL2-a-0.91520-hmf.txt'
    MDPL2_hmf_file='/disk1/salam/Projects/HODFitter/MDPL2-nosubhalo-a-0.91520-hmf.txt'
    MDPL2_hmf=np.loadtxt(MDPL2_hmf_file)
    
    #
    mhalo_range=MDPL2_hmf[:,0]
    count_halo=MDPL2_hmf[:,1]
    nMhalo=mhalo_range.size
    for ii in range(nMhalo):
        NBCG=hpop.MeanBCG({'LMcut':tchain[:,tdict['LMcut']],'sigma':tchain[:,tdict['sigma']]},mhalo_range[ii])
        Nsat=getNsat(tchain,tdict,mhalo_range[ii],NBCG)

        #multiply by halo mass function
        NBCG=NBCG*count_halo[ii]
        Nsat=Nsat*count_halo[ii]
        
        if(ii==0):
            #store the mean and std of NBCG
            NBCG_stat=np.zeros(nMhalo*2).reshape(nMhalo,2)
            #store the total number of NBCG
            NBCG_tot_real=np.zeros(NBCG.size)
            #store the mean and std of Nsat
            Nsat_stat=np.zeros(nMhalo*2).reshape(nMhalo,2)
            #store total number of satelit
            Nsat_tot_real=np.zeros(NBCG.size)

        #for central galaxy
        NBCG_stat[ii,0]=np.mean(NBCG)
        NBCG_stat[ii,1]=np.std(NBCG)
        NBCG_tot_real=NBCG_tot_real+NBCG        
        #for satellite galaxies
        Nsat_stat[ii,0]=np.mean(Nsat)
        Nsat_stat[ii,1]=np.std(Nsat)
        Nsat_tot_real=Nsat_tot_real+Nsat

    
    if(plots==1):
        pl.errorbar(mhalo_range,NBCG_stat[:,0],yerr=NBCG_stat[:,1])
        pl.errorbar(mhalo_range,Nsat_stat[:,0],yerr=Nsat_stat[:,1])
        pl.xscale('log')
        pl.yscale('log')
    
    Ngal_tot=[np.mean(NBCG_tot_real+Nsat_tot_real),np.std(NBCG_tot_real+Nsat_tot_real)]
    Ncen_tot=[np.mean(NBCG_tot_real),np.std(NBCG_tot_real)]
    Nsat_tot=[np.mean(Nsat_tot_real),np.std(Nsat_tot_real)]
    Fsat=[np.mean(1.0*Nsat_tot_real/(NBCG_tot_real+Nsat_tot_real)),
          np.std(1.0*Nsat_tot_real/(NBCG_tot_real+Nsat_tot_real))]
    
    if(outwrite!=''):
        fout=open(outwrite,'w')
        fout.write('#Ngal= %10.5g %10.5g\n'%(Ngal_tot[0],Ngal_tot[1]))
        fout.write('#Ncen= %10.5g %10.5g\n'%(Ncen_tot[0],Ncen_tot[1]))
        fout.write('#Nsat= %10.5g %10.5g\n'%(Nsat_tot[0],Nsat_tot[1]))
        fout.write('#sat_fraction= %10.5g %10.5g\n'%(Fsat[0],Fsat[1]))
        fout.write('#halo mass, Nbcg, std_Nbcg, Nsat, std_Nsat\n')
        np.savetxt(fout,np.column_stack([mhalo_range,NBCG_stat,Nsat_stat]))
        fout.close()
    
    return Fsat


def Write_1D_2D_density(chain_file,ext='',tag='',burnin=0.3,nx=100,ny=100,parlist=[],
                            outdir='/disk1/salam/Projects/GAMA/SmallScaleRSD_data/',correct_gHV_gIHV=0):
    '''This takes a chain file and list of parameters to make write the density
    for all parameters and write to appropriate file, it also writes marginalized values
    when correct_gHV_gIHV=1 then the chains are corrected for gHV due to mean redshift anf gIHV due to 3d to 1d velocity dispersion conversion'''
    
    #first load the chain file
    tchain,tdict=load_chains(chainroot=chain_file,ext=ext,nchain=1,burnin=burnin)

   
    if(correct_gHV_gIHV==1):
        if('gammaIHV' in tdict.keys()):
            print('Multiplying gammaIHV by sqrt(3) to account for 1D velocity')
            tchain[:,tdict['gammaIHV']]=np.sqrt(3)*tchain[:,tdict['gammaIHV']]

        if('gammaHV' in tdict.keys()):
            print('Multiplying gammaHV by 1.1*ratio of growth rate data to sim to account shift in mean redshift and extra factor of 1+z in the code where z=0.1 was used')
            #redshift of simulation used only for GAMA fitting
            zsim=0.1
            #mean redshift of the sample
            tspl=chain_file.split('-zlim-')[1]
            tspl_z=tspl.split('-')
            zdata=0.5*(np.float(tspl_z[0])+np.float(tspl_z[1]))
            #Assuming planck cosmology
            Ho=70;omM=0.3;omL=1.0-omM;
            omMz=mcosm.omMz(np.array([zsim,zdata]),Ho,omM,omL)
            fz=np.power(omMz,0.55)
            #correcting by the ratio of growths
            tchain[:,tdict['gammaHV']]=1.1*tchain[:,tdict['gammaHV']]*fz[0]/fz[1]

    


    if(parlist==[]):
        parlist=list(tdict.keys())
        
    #aaply non NAN and non -infinity selection for loglike
    ind1=np.isfinite(tchain[:,1])
    ind2=~np.isnan(tchain[:,1])
    tchain=tchain[ind1*ind2,:]
    
    #directory for results
    res_dir='%s%s/'%(outdir,tag)
    #check if directory exists otherwise create a new directory
    if(not os.path.isdir(res_dir)):
        os.system('mkdir %s'%res_dir)
    #get the occupation probability
    #Fsat=HOD_occupation(tchain,tdict,plots=0,outwrite='%s/occupation_numbers.txt'%res_dir)
    for pp ,par in enumerate(parlist):
        outroot='%s%s'%(res_dir,par)
        res_stat=plot_1dlike(xsample=tchain[:,tdict[par]],nx=nx, bandwidth=1.0/9.0,xlims=Get_par_prior(parin=par),
                outroot=outroot,plots=-1)

        for pp2 ,par2 in enumerate(parlist[:pp]):
            outroot2='%s-%s'%(outroot,par2)
            plot_2dcontours(xysample=tchain[:,[tdict[par],tdict[par2]]],nx=nx,ny=ny, bandwidth=1.0/9.0,
                xlims=Get_par_prior(parin=par),ylims=Get_par_prior(parin=par2),outroot=outroot2,plots=-1)
            

    return 
